<?php 
require "utilities/helpFunctions.php";
require "classes/ConnexionDatabase.php";
require "classes/ValidationFormulaire.php";
require "classes/Article.php";

$validator = new ValidationFormulaire();
$bdd = new ConnexionDatabase();

if( !empty($_GET['ref']) )
{
    // Select the article 
    $article = $bdd->selectArticleByReference($_GET['ref'])[0];

    extract($article);

    if( isset( $_POST['update-article'] ) )
    {
        extract($_POST);
        $fieldsToValidate = [
            'titre' => [$title, ['required', 'alphaCustom'] ],
            'price' => [$price, ['required', 'number'] ],
            'quantity' => [$quantity,['required', "integer"] ] 
        ]; 
        $validator->setAttributes($fieldsToValidate);
        $errors = $validator->validate()->getErrors();
        
        if( !$errors )
        {
            $newArticle = new Article($title, $price, $quantity);
            // DB query update article
            $success = $bdd->updateArticleByReference( $_GET['ref'], $newArticle );
            if( $success )
            {
                header("location: index.php");
            }
            else
            {
                $errors["Update-article"] = "Erreur lors de la mise à jour de l'article !";
            }
        }
    }
    require "views/update-article.view.php";
}
else
{
    header("location: index.php");
}
