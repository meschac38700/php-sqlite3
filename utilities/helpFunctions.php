<?php

if( !function_exists("errorMessages") )
{
    /**
     * @param Array $error [ 'key' => ['error', 'error'] ] or ['key' => 'error' ]
     */
    function errorMessages($errors)
    {
        $errormsg = "<ul>";
        foreach($errors as $key => $error)
        {
            $errormsg .= "<li>".$key."<ul>";             
            if( is_array($error) )
            {
                foreach($error as $msg)
                {
                    $errormsg .= "<li>".$msg."</li>";
                }
                $errormsg .= "</ul></li>";
            }
            else
            {
                $errormsg .= "<li>".$error."</li></ul></li>";             
            }
        }
        $errormsg .= "</ul>";
        return $errormsg;
    }
}