(function()
{   
    var ajax = function ( $url, $data )
    {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                window.location = "/index.php";
            }
        };
        xhttp.open("POST", $url, true);
        xhttp.send($data);
    
    }

    // let updateArticle = document.querySelectorAll('#update-article');
    // updateArticle.forEach(function($element )
    // {
    //     $element.addEventListener("click", function(e)
    //     {
    //         e.preventDefault();
    //         var data = new FormData();
    //         data.append('reference', this.dataset.ref );
    //         ajax('/update-article.php', data);
    //     })
    // })

    let deleteArticle = document.querySelectorAll('#delete-article');
    deleteArticle.forEach(function( $element )
    {
        $element.addEventListener("click", function(e)
        {
            e.preventDefault();
            var data = new FormData();
            data.append('ref', this.dataset.ref );
            ajax('/delete-article.php', data);
        })
    });
    

})();