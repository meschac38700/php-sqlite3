<div class="menu">
    <form method="GET">
        <div class="form-group">
            <input type="text" name="title" id="title" class="form-control" placeholder="Rechercher par titre">
        </div>
        <button type="submit" value="" name="search-title" class="btn btn-primary center">Par titre</button>
    </form>
    
    <form method="GET">
        <div class="form-group">
            <input type="text" name="ref" id="ref" class="form-control" placeholder="Rechercher par référence">
        </div>
        <button type="submit" value="" name="search-ref" class="btn btn-primary center">Par référence</button>
    </form>
    <form method="GET">
        <div class="form-group">
            <div class="flex">
                <select class="start" name="minPrice">
                    <?php for($i=0; $i < 1000; $i++):?>
                        <?php if($i == 0):?>
                            <option selected value="<?= $i ?>"><?= $i ?></option>
                        <?php else: ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endif;?>
                    <?php endfor; ?>
                </select>
                <select class="end" name="maxPrice">
                    <?php for($i=0; $i < 1000; $i++):?>
                        <?php if($i == 0):?>
                            <option selected value="<?= $i ?>"><?= $i ?></option>
                        <?php else: ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endif;?>
                    <?php endfor; ?>
                </select>
            </div>
        </div>
        <button type="submit" value="" name="search-price" class="btn btn-primary center">Par interval de prix</button>
    </form>
</div><!--  end .menu   -->