<?php require "views/partials/_header.php" ?>
<?php require "views/partials/_nav.php" ?>
<main class="main">
    <h1 class="text-center">Modifier l'article <span class="article-ref"><?= $reference ?></span></h1>
    <div class="alert-danger">
        <?= errorMessages($errors) ?>
    </div>
    <form class="form" method="POST" novalidate>
        <div class="form-group">
            <label for="title" class="form-label">Titre</label>
            <input type="text" value="<?= $titre ?>" name="title" placeholder="Article title" required class="form-control" id="title">
        </div>
        <div class="form-group">
            <label for="price" class="form-label">Prix</label>
            <input type="text" value="<?= $prix ?>" name="price" placeholder="Article price ex: 20.99" required class="form-control" id="price">
        </div>
        <div class="form-group">
            <label for="quantity" class="form-label">Quantité</label>
            <input type="text" value="<?= $quantite ?>" min="0" pattern="^[0-9]+$" name="quantity" placeholder="Article quantity ex: 20" required class="form-control" id="quantity">
        </div>
        <div class="form-group mt-40">
            <input type="submit" value="Modifier article" class="btn btn-primary" name="update-article">
        </div>
        
    </form>
</main><!--  end .main   -->

<?php require "views/partials/_footer.php" ?>