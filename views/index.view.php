<?php require "views/partials/_header.php" ?>
<?php require "views/partials/_nav.php" ?>
<?php require "views/partials/_menu.php" ?>
<main class="main">
    <h1 class="text-center" style="text-decoration: underline; margin: 20px 0;">La liste de tous les articles</h1>
    <div class="articles">
        <?php if( $articles ): ?>
            <table class="table" id="customers">
                <thead>
                    <tr>
                        <th>Référence</th>
                        <th>titre</th>
                        <th>Quantité</th>
                        <th>Prix</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($articles as $key => $article): ?>
                        <tr>
                            <td><?= $article['reference'] ?></td>
                            <td><?= $article['titre'] ?></td>
                            <td><?= $article['quantite'] ?></td>
                            <td><?= $article['prix'] ?> €</td>
                            <td>
                                <a href="#" data-ref="<?= $article['reference'] ?>" id="delete-article"><i class="far fa-trash-alt"></i></a>
                                <a href="/update-article.php?ref=<?= $article['reference'] ?>" id="update-article"><i class="far fa-edit"></i></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php elseif( $errors ): ?>
        <div class="alert-danger">
            <?= errorMessages($errors) ?>
        </div>
        <a href="ajouter-article.php" class="btn btn-primary">Ajout un article</a>
        <?php else: ?>
            <p class="alert-danger">Aucun article de displonnible</p>
            <a href="ajouter-article.php" class="btn btn-primary">Ajout un article</a>
        <?php endif; ?>
    </div>
</main><!--  end .main   -->

<?php require "views/partials/_footer.php" ?>