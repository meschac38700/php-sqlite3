 Premier exo: 
__**Objectif : Manipuler les collections de type Liste.**__

__Soit � d�velopper une application pour la gestion d�un stock.__

Un article est caract�ris� par son num�ro de r�f�rence, son nom, son prix de vente et une quantit� en stock.

Le stock est repr�sent� par une collection d�articles.
Travail � faire:

Cr�er la classe article contenant les �l�ments suivants :

    Les attributs/propri�t�s.
    Un constructeur d�initialisation.
    La m�thode ToString().

Dans la classe Program cr�er :

Le stock sous forme d'une collection d�articles de votre choix.

Un menu pr�sentant les fonctionnalit�s suivantes :


   -  Rechercher un article par r�f�rence.
   - Ajouter un article au stock en v�rifiant l�unicit� de la r�f�rence.
   - Supprimer un article par r�f�rence.
   - Modifier un article par r�f�rence.
   - Rechercher un article par nom.
   - Rechercher un article par intervalle de prix de vente.
   - Afficher tous les articles.
   - Quitter
    //Source : www.exelib.net