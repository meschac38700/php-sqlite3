<?php

class Store
{
    private $store;

    /**
     * @param void
     * @return void
     * Initialize a store
     */
    public function __construct()
    {
        $this->store = [];
    }

    /**
     * check if a reference has not already been assigned to an article
     * @param String $reference
     * @return Bool
     */
    private function isUniqueReference( String $reference)
    {
        foreach($this->store as $article)
        {
            if($article->getReference() == $reference)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * check if store contains the articlce before add it to the store 
     * @param Article $article
     * @return void
     */
    public function addArticle(Article $article)
    {
        if( $this->isUniqueReference( $article->getReference() ) )
        {
            array_push($this->store, $article);
        }
    }

    /**
     * Delete an article from the store 
     * @param String $reference
     * @return void
     */
    public function deleteByReference(String $reference)
    {
        foreach($this->store as $key => $article)
        {
            if( $article->getReference() == $reference)
            {
                unset($this->store[$key]);
            }
        }
    }

    /**
     * @param void
     * @return Array the current store
     */
    public function getAllArticle()
    {
        return $this->store;
    }

    /**
     * @param String reference
     * @return Article $article
     */
    public function getArticleByReference(String $reference)
    {
        foreach($this->store as $key => $article)
        {
            if( $article->getReference() == $reference)
            {
                return $article;
            }
        }
    }
    
    /**
    * @param String $name
    * @return Article $article 
    */
    public function getArticleByName(String $name)
    {
        foreach($this->store as $key => $article)
        {
            if( $article->getNom() == $nom)
            {
                return $article;
            }
        }
    }

    /**
     * @param int $start
     * @param int $end
     * @return Array list of some article or empty list []
     */
    public function getArticleByPriceInterval(int $start, int $end)
    {
        $newstore = [];
        foreach($this->store as $key => $article)
        {
            if( $article->getPrix() >= $start && $article->getPrix() <= $end)
            {
                array_push($newstore, $article);
            }
        }
        return $newstore;
    }

    /**
     * Update an article
     * @param String $reference
     * @param Article $article
     * @return void
     */
    public function updateArticleByReference(String $reference, Article $newArticle)
    {
        foreach($this->store as $key => $article)
        {
            if( $article->getReference() == $reference)
            {
                $this->store[$key] = $newArticle;
                return;
            }
        }
    }

}