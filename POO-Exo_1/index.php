<?php
require "Article.php";
require "Store.php";

//Create a new store
$store = new Store();

// Create 10 articles
for($i=0; $i < 10; $i++)
{
    //New article
    $a = new Article("T-shirt_".($i+1), (15.99+$i), (5+$i));
    // add the new article to the store 
    $store->addArticle($a);
}


// update the first article
$newArticle = new Article("Jean", 10.99, 120);
$store->updateArticleByReference( $store->getAllArticle()[0]->getReference(), $newArticle );

// Delete third article
$store->deleteByReference($store->getAllArticle()[2]->getReference());


// display all article
foreach($store->getAllArticle() as $key => $article)
{
    // call toString, article representation method
    echo $article->toString();
}
