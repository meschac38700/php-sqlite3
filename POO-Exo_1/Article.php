<?php 

class Article
{
    private $reference;
    private $nom;
    private $prix;
    private $quantite;

    /**
     * @param String $nom 
     * @param float $prix
     * @param int $quantite
     * @return void
     */
    public function __construct($nom, $prix, $quantite)
    {
        $this->reference = uniqid('artcile_');
        $this->nom = $nom;
        $this->prix = $prix;
        $this->quantite = $quantite;
    }

    /**
     * @param void
     * @return String Représentation de d'un article
     */
    public function toString() : String
    {
        return $this->nom . "\n<br>\t" . "Reference : ".$this->reference . "\n<br>\t" . "Quantité : ". $this->quantite . "\n<br>\t" . "Prix : ". $this->prix."€". PHP_EOL . "<br><hr>"; 
    }

    /**
     * @param void
     * @return String article name
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param void
     * @return String article reference
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param void
     * @return float article price
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param void
     * @return int article quantity
     */
    public function getQuantite()
    {
        return $this->quantite;
    }
}

