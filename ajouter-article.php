<?php
require "utilities/helpFunctions.php";
require "classes/Article.php";
require "classes/ConnexionDatabase.php";

$errors = null;
extract($_POST);
if( isset( $_POST['add-article'] ) )
{
    require "classes/ValidationFormulaire.php";
    
    
    $fieldsToValidate = [
        "title" => [$title, ["required", "alphaCustom"] ],
        "price" => [$price, ["required", "number"] ],
        "quantity" => [$quantity, ["required", "integer"] ],
    ];
    $validator = new ValidationFormulaire();
    $validator->setAttributes($fieldsToValidate);
    $errors = $validator->validate()->getErrors();
    
    if(!$errors )
    {
        try
        {
            $article = new Article($title, $price, $quantity);
            $bdd = new ConnexionDatabase();
            $bdd->insertArticle($article);
            header("location: index.php");
        }
        catch(Exception $e)
        {
            echo "Errors 500! impossible d'inserer un(e) ". get_class($article);
            exit();
        }
    }
    
}    

require "views/ajouter-article.view.php";