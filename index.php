<?php
require "utilities/helpFunctions.php";
require "classes/ConnexionDatabase.php";
require "classes/ValidationFormulaire.php";
$articles = [];
$validator = new ValidationFormulaire();
$bdd = new ConnexionDatabase();

extract($_GET);
if( isset( $_GET['search-title'] ) )
{
    $fieldsToValidate = ['titre' => [$title, ['required', 'alphacustom'] ] ]; 
    $validator->setAttributes($fieldsToValidate);
    $errors = $validator->validate()->getErrors();
    
    if( !$errors )
    {
        // DB query
        $articles = $bdd->selectArticleByTitle( $title );
    }
}
elseif( isset($_GET['search-ref'] ) )
{
    $fieldsToValidate = ['reference' => [$ref, ['required', 'alphanumeric'] ] ]; 
    $validator->setAttributes($fieldsToValidate);
    $errors = $validator->validate()->getErrors();

    if( !$errors )
    {
        // DB query
        $articles = $bdd->selectArticleByReference( $ref );
    }
}
elseif( isset($_GET['search-price'] ) )
{    
    $fieldsToValidate = ['min-price' => [$minPrice, ['required', 'integer'] ], 'max-price' => [$maxPrice,['required', "integer"] ] ]; 
    $validator->setAttributes($fieldsToValidate);
    $errors = $validator->validate()->getErrors();

    if( !$errors )
    {
        // DB query
        $articles = $bdd->selectByPriceInterval( $minPrice, $maxPrice );
    }
}
else
{
    $articles = $bdd->selectAllArticles( );
}



require "views/index.view.php";