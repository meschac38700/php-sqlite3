<?php 
class Article
{
    private $reference;
    private $titre;
    private $prix;
    private $quantite;
    private $_sql = "CREATE TABLE IF NOT EXISTS articles(
                    id integer primary key,
                    reference varchar(255) unique,
                    titre varchar(100),
                    quantite integer,
                    prix float
                );";

    public function __construct($titre, $prix, $quantite)
    {
        $this->reference = uniqid();
        $this->titre = $titre;
        $this->prix = $prix;
        $this->quantite = $quantite;
    }

    public function toArray() : array
    {
        $array = [];
        foreach ($this as $key => $value) 
        {
            $array[$key] = $value;
        }
        return $array;
    }
    
    public function toString() : string
    {
        return $this->titre . "\n\t" . "Reference : ".$this->reference . "\n\t" . "Quantité : ". $this->quantite . "\n\t" . "Prix : ". $this->prix; 
    }

    public function getTitre() : string
    {
        return $this->titre;
    }

    public function getReference() : string
    {
        return $this->reference;
    }
    public function getPrix() : float
    {
        return $this->prix;
    }

    public function getQuantite() : int
    {
        return $this->quantite;
    }

    public function getSQL() : string
    {
        return $this->sql;
    }
   

}

