<?php

class Model implements Iterator
{
    private $var = [];

    public function __construct()
    {

    }

    function iterateVisible() {
        
        foreach ($this as $key => $value) {
            echo "$key => $value"."<br>";
        }
    }

    public function rewind()
    {
        reset($this->var);
    }

    public function current()
    {
        $var = current($this->var);
        return $var;
    }

    public function key()
    {
        $var = key($this->var);
        return $var;
    }

    public function next()
    {
        $var = next($this->var);
        return $var;
    }

    public function valid()
    {
        $key = key($this->var);
        $var = ($key !== NULL && $key !== FALSE);
        echo "valide : $var\n";
        return $var;
    }

    public function toArray() : array
    {
        foreach ($this as $key => $value) 
        {
            $this->array["$key"] = $value;
        }
        return $this->array;
    }

}