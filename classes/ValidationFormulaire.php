<?php
class ValidationFormulaire
{
    private $errors;
    private $fields;
    private $fieldName;
    private $fieldValue;
    //private $valid_regles = ["required", "alphaonly", "integer", "float", "number", "alphanumeric", "email", "url"];
    
    /**
     * @param Array $p_fields = [
     *                'inputName'=>[
     *                   'inputValue', [ 'regle_1', 'regle_2' ]
     *                 ]
     *              ]
     * @return void
     */
    public function setAttributes(Array $p_fields)
    {
        $this->fields = $p_fields;
        $this->errors = [];
    }

    public function getErrors()
    {
        return $this->errors;
    }


    public function validate()
    {
        foreach($this->fields as $key => $value)
        {
            // Initialize attributes
            $this->fieldName = $key;
            $this->fieldValue = $value[0];
            
            foreach($value[1] as $regle)
            {
                switch( strtolower($regle) )
                {
                    case "required":
                        $this->isRequired();
                        break;
                    case "alphaonly":
                        $this->alphaOnly();
                        break;
                    case "alphacustom":
                        $this->alphaCustom();
                        break;
                    case "number":
                        $this->isNumber();
                        break;
                    case "float":
                        $this->isDouble();
                        break;
                    case "integer":
                        $this->isInteger();
                        break;
                    case "alphanumeric":
                        $this->alphaNumeric();
                        break;
                    case "email":
                        $this->isEmail();
                        break;
                    case "url":
                        $this->isUrl();
                        break;
                }
            }
        }
        return $this;
    }

    /**
     * set a error message
     * @param String $message
     * @return void
     */
    private function setMessage($message)
    {
        // Check si le champs comporte deja une erreur
        if( array_key_exists( $this->fieldName, $this->errors ) )
        {
            
            if( is_array( $this->errors[$this->fieldName] ) )
            {
                array_push($this->errors[$this->fieldName], $message );
            }
            else
            {
                $tmp = $this->errors[$this->fieldName];
                $this->errors[$this->fieldName] = [];
                array_push($this->errors[$this->fieldName], $tmp, $message );
            }

        }
        else 
        {
            // Sinon on ajoute simplement un message d'erreur
            $this->errors[$this->fieldName] = $message;   
        }
    }

    /**
     * Validation d'un champs obligatoire
     * @param void
     * @return void
     */
    private function isRequired()
    {

        if( $this->fieldValue != "0" && empty( trim($this->fieldValue) ) )
        {
            $this->setMessage("Le champs ". $this->fieldName . " ne peut être vide !");
        }
    }

    /**
     * Validation d'un champs alpha
     * @param void
     * @return void
     */
    private function alphaOnly()
    {
        $success = preg_match('/^[a-z]+$/i', $this->fieldValue);
        if( !$success )
        {
            $this->setMessage( "Le champs ". $this->fieldName . " doit contenir uniquement des lettres !" );
        }
    }

    /**
     * Autorise seulement les lettre, chiffres, _ et -
     */
    private function alphaCustom()
    {
        $success = preg_match('/^[a-z0-9\-_]+$/i', $this->fieldValue);
        if( !$success )
        {
            $this->setMessage( "Le champs ". $this->fieldName . " ne peut contenir que des lettres et des chiffre ainsi que les tirêts ( - ) ou les underscores ( _ )" );
        }
    }

    /**
     * Validation d'un champs numeric
     * @param void
     * @return bool
     */
    private function isInteger( $message=true )
    {
        $success = preg_match('/^[0-9]+$/i', $this->fieldValue);
        if( !$success )
        {
            if($message)
            {
                $this->setMessage( "Le champs ". $this->fieldName . " doit être un nombre entier !" );
            }
            return false;
        }
        return true;
    }


    /**
     * Validation d'un champs flottant (decimal)
     * @param void
     * @return bool
     */
    private function isDouble( $message=true )
    {
        $success = preg_match('/^[0-9]+\.[0-9]+$/i', strval($this->fieldValue) );
        if( !$success )
        {
            if( $message )
            {
                $this->setMessage( "Le champs ". $this->fieldName . " doit être un nombre decimal ( ex: 1.5 ) !" );
            }
            return false;
        }
        return true;
    }

    /**
     * Validation d'un champs flottant ou entier
     * @param void
     * @return bool
     */
    private function isNumber()
    {
        if( !$this->isInteger( false ) && !$this->isDouble( false ) )
        {
            $this->setMessage("Le champs ". $this->fieldName . " doit être un nombre entier ou decimal!");
        }        
    }

    /**
     * Validation d'un champs lettre + nombre ou juste nombre ou just lettre
     * @param void
     * @return void
     */
    private function alphanumeric()
    {
        $success = preg_match('/^[a-z0-9]+$/i', strval($this->fieldValue) );
        if( !$success )
        {
            $this->setMessage( "Le champs ". $this->fieldName . " ne doit contenir que des lettres et des chiffres !" );   
        }
    }

    /**
     * Validation d'un champs email
     * @param void
     * @return void
     */
    private function isEmail()
    {
        $success = preg_match('/^([a-z]+)([\-_\.]?[a-z0-9]+[\-_\.]?)+?@[a-z]+([\-_]?[a-z]+)+?(\.[a-z]{2,})/i', strtolower($this->fieldValue) );
        if ( !$success )
        {
            $this->setMessage("Le champs ". $this->fieldName . " ne contient pas une addresse email valide !");
        }
    }
    
    /**
     * Validation d'un champs url
     * @param void
     * @return void
     */
    public function isUrl()
    {
        $success = preg_match('/((http(s)?|ftp):\/\/)?([0-9]{3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}|[\w]+([\.\-_]?[\w]+)?(\.[\w]{2,}))(\:[0-9]{4})?\/?(.+\S)?/i', $this->fieldValue);
        if( !$success)
        {
            $this->setMessage("Le champs ". $this->fieldName . " ne contient pas une addresse URL valide !");
        }
    }
}