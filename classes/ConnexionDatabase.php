<?php
// json_decode(json_encode($booking), true); => Array
//json_decode(json_encode($booking)); => stdClass
class ConnexionDatabase
{
    private static $bdd;
    private $results;

    public function __construct()
    {
        self::getBDD();
        $this->createArticleTable();
        $this->results = [];
    }

    /**
     * stdClass to array
     */
    // public function arrayCastRecursive($array)
    // {
    //     if (is_array($array)) {
    //         foreach ($array as $key => $value) {
    //             if (is_array($value)) {
    //                 $array[$key] = arrayCastRecursive($value);
    //             }
    //             if ($value instanceof stdClass) {
    //                 $array[$key] = arrayCastRecursive((array)$value);
    //             }
    //         }
    //     }
    //     if ($array instanceof stdClass) {
    //         return arrayCastRecursive((array)$array);
    //     }
    //     return $array;
    // }
    
    /**
    * Méthode qui crée l'unique instance de la variable $bdd
    * si elle n'existe pas encore puis la retourne.
    *
    * @param void
    * @return Singleton $bdd
    */
    public static function getBDD() {
    
        if(is_null(self::$bdd)) 
        {
            self::$bdd = new SQLite3(__DIR__.'/db/Database.db');  
        }

        return self::$bdd;
    }

    /*
        Create a new table by giving a sql request
    */
    public static function createTable($sql)
    {
        try
        {
            $self::$bdd->exec($sql);
        }
        catch( Exception  $e)
        {
            echo "Impossible de créer la table ! ";
            echo $e->getMessage();
            exit();
        }
    }

    /*
        Create an article table
    */
    private function createArticleTable()
    {
        try
        {
            $sql = "CREATE TABLE IF NOT EXISTS articles(
                id integer primary key,
                reference varchar(255) unique,
                titre varchar(100),
                quantite integer,
                prix float);";
            
            self::$bdd->exec($sql);
        }
        catch(Exception $e)
        {
            echo "Impossible de créer la table article !";
            echo $e->getMessage();
            exit();
        }
    }

    /*
        Insert an article
    */
    public function insertArticle(Article $article)
    {
        $sql = 'INSERT INTO articles(reference, titre, quantite, prix) 
                VALUES(:ref, :titre, :qt, :prix);';
        $stmt = self::$bdd->prepare($sql);
        
        $stmt->bindParam(':ref', $article->getReference());
        $stmt->bindParam(':titre', $article->getTitre());
        $stmt->bindParam(':qt', $article->getQuantite());
        $stmt->bindParam(':prix', $article->getPrix());
        $result = $stmt->execute();
        return $result;
    }

    /*
        Get all articles
    */
    public function selectAllArticles()
    {
        $sql = 'SELECT * FROM articles ';

        $query = self::$bdd->query($sql);

        while ($row = $query->fetchArray()) {

            array_push($this->results, $row);
        }
        return $this->results;
    }

    /*
        Get an article by his reference
    */
    public function selectArticleByReference($reference)
    {
        $sql = 'SELECT * FROM articles WHERE upper(reference)="'. strtoupper($reference) .'"';
        $result = self::$bdd->query($sql);
        while ($row = $result->fetchArray()) {

            array_push($this->results, $row);
        }
        return $this->results;
    }

    /*
        Get an article by his name
    */
    public function selectArticleByTitle($title)
    {
        $sql = 'SELECT * FROM articles WHERE upper(titre)="'.strtoupper($title) .'"';
        $result = self::$bdd->query($sql);
        while ($row = $result->fetchArray()) {

            array_push($this->results, $row);
        }
        return $this->results;
    }

    /*
        Get artcile by price interval
    */

    public function selectByPriceInterval($start, $end)
    {
        $sql = "SELECT * FROM articles WHERE articles.prix BETWEEN ". intval($start). " and ". intval( $end). ";";
        $query = self::$bdd->query($sql);
        //$query = self::$bdd->unbufferedQuery($sql);
        //$result = $query->fetchArray();
        while ($row = $query->fetchArray()) {

            array_push($this->results, $row);
        }
        return $this->results;
    }

    /*
        Delete an article
    */
    public function dropArticleByReference($reference)
    {
        $sql = 'DELETE FROM articles WHERE upper(reference)="' . strtoupper($reference) . '"';
        $result = self::$bdd->query($sql);
        return $result;
    }

    /*
        Update an article
    */
    public function updateArticleByReference($reference, $newArticle)
    {
        $sql = "UPDATE articles SET titre=:titre, quantite=:qt, prix=:prix WHERE upper(reference)=:ref";
        $stmt = self::$bdd->prepare($sql);
        $stmt->bindParam(':titre', $newArticle->getTitre());
        $stmt->bindParam(':qt', $newArticle->getQuantite());
        $stmt->bindParam(':prix', $newArticle->getPrix());
        $stmt->bindParam(':ref', strtoupper($reference) );
        $result = $stmt->execute();
        return $result;
    }

}



//$a = new Article("Legging Addidas", 30, 50);


// $c = new ConnexionDatabase();
// $result = $c->selectByPriceInterval(50,10);
// var_dump($result);
// print_r($result);

// //var_dump(count($result));
// die();


// $obj = new stdClass;
// $obj->aaa = 'asdf';
// $obj->bbb = 'adsf43';
// $arr = array('asdf', array($obj, 3));

// var_dump($arr);
// $arr = arrayCastRecursive($arr);
// var_dump($arr);
// die();



// save code


    //get_class($a)
        // foreach($a->toArray() as $key => $value) {
        //     $result = preg_match( "%^_%", $key, $matches );
           
        //     if(!$result)
        //     {
        //         echo "No match<br/>";
        //     }
        //     else
        //     {
        //         echo $key;
        //     }
        //     //echo "$key => $value\n";
        // }